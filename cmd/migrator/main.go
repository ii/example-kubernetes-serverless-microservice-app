package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	// include Pg
	_ "github.com/lib/pq"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/database"
)

func main() {
	var db *sql.DB
	log.Printf("migrate starting")
	canConnect := false
	for canConnect == false {
		dbConn, err := database.NewDatabase().Open()
		if err != nil {
			log.Println("unable to connect to database. Waiting...", err)
			time.Sleep(time.Second * 5)
			continue
		}
		db = dbConn
		canConnect = true
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	log.Println("Connected to the database")
	time.Sleep(time.Second * 2)

	log.Println("migrate running")
	m, err := migrate.NewWithDatabaseInstance("file:///var/run/ko/migrations", "postgres", driver)
	if err != nil {
		log.Printf("migrate prepare failed: %v", err)
		os.Exit(1)
	}
	err = m.Up()
	if err != nil {
		if err.Error() == "no change" {
			log.Println("no change made by migration scripts")
		} else {
			log.Printf("migrate failed: %v", err)
			os.Exit(1)
		}
	}
	log.Printf("migrate complete")
}
