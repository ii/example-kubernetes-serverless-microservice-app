begin;

insert into product (name, price, size, comment) values
(
'Ethiopia',
10,
'1kg',
'Fresh and dark roasted'
),
(
'Equador',
11,
'1kg',
'Light and floral'
),
(
'Peru',
12,
'1.1kg',
'Light and dark roasted'
);

commit;
