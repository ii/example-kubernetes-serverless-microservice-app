begin;

create table if not exists orders (
  id text default md5(random()::text || clock_timestamp()::text)::uuid not null,
  comment text not null,
  userid text not null,
  productid text not null,
  fulfilled boolean not null default false,
  creationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  modificationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  deletionTimestamp int not null default 0,

  primary key (id),
  foreign key (userid) references users(id),
  foreign key (productid) references product(id)
);

comment on table orders is 'The orders table for storing orders of product';

commit;
