begin;

create table if not exists product (
  id text default md5(random()::text || clock_timestamp()::text)::uuid not null,
  name text not null,
  price money not null,
  size text not null,
  comment text not null,
  creationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  modificationTimestamp int not null default date_part('epoch',CURRENT_TIMESTAMP)::int,
  deletionTimestamp int not null default 0,

  primary key (id)
);

comment on table product is 'The product table for storing products';

commit;
