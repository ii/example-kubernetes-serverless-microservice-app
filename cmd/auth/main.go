package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt"
	"github.com/google/uuid"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/common"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/service"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/system"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/types"
)

var (
	jwtAlg *jwt.SigningMethodHMAC = jwt.SigningMethodHS256
)

type Auth struct {
	SystemManager *system.SystemManager
}

func (a *Auth) GenerateJWTauthToken(id string, authNonce string, expiresIn time.Duration) (tokenString string, err error) {
	if expiresIn == 0 {
		expiresIn = 24 * 5
	}
	secret, err := a.SystemManager.GetJWTsecret()
	if err != nil {
		return "", err
	}
	expirationTime := time.Now().Add(time.Hour * expiresIn)
	token := jwt.NewWithClaims(jwtAlg, types.JWTclaim{
		ID:        id,
		AuthNonce: authNonce,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	})

	tokenString, err = token.SignedString([]byte(secret))
	return tokenString, err
}

func (a *Auth) ValidateJWTauthToken(r *http.Request) (valid bool, tokenClaims *types.JWTclaim, err error) {
	secret, err := a.SystemManager.GetJWTsecret()
	if err != nil {
		return false, &types.JWTclaim{}, err
	}
	tokenHeaderJWT, err := GetAuthTokenFromHeader(r)
	if err != nil {
		return false, &types.JWTclaim{}, err
	}
	claims := &types.JWTclaim{}
	token, err := jwt.ParseWithClaims(tokenHeaderJWT, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		return false, &types.JWTclaim{}, err
	}

	if token.Valid == false {
		return false, &types.JWTclaim{}, fmt.Errorf("Unable to use existing login token as it is invalid")
	}
	if err := token.Claims.Valid(); err != nil {
		return false, &types.JWTclaim{}, fmt.Errorf("Unable to use existing login token as it is invalid")
	}
	if token.Method.Alg() != jwtAlg.Alg() {
		return false, &types.JWTclaim{}, fmt.Errorf("Unable to use login token provided, please log in again")
	}

	reqClaims, ok := token.Claims.(*types.JWTclaim)
	if ok == false {
		return false, &types.JWTclaim{}, fmt.Errorf("Unable to read JWT claims")
	}
	return true, reqClaims, err
}

func GetAuthTokenFromHeader(r *http.Request) (string, error) {
	tokenHeader := r.Header.Get("Authorization")
	if tokenHeader == "" {
		return "", fmt.Errorf("Unable to find authorization token (header doesn't exist)")
	}
	authorizationHeader := strings.Split(tokenHeader, " ")
	if authorizationHeader[0] != "bearer" || len(authorizationHeader) <= 1 {
		return "", fmt.Errorf("Unable to find authorization token (must be as bearer)")
	}
	return authorizationHeader[1], nil
}

func (a *Auth) GetAuth(db *sql.DB) http.HandlerFunc {
	a.SystemManager.DB = db
	return func(w http.ResponseWriter, r *http.Request) {
		valid, claims, err := a.ValidateJWTauthToken(r)
		if err != nil {
			common.FailMessage(types.FailMessage{
				Message:    err.Error(),
				StatusCode: http.StatusBadRequest,
			}).ServeHTTP(w, r)
			return
		}
		if !valid {
			common.FailMessage(types.FailMessage{
				Message:    "Token is invalid",
				StatusCode: http.StatusBadRequest,
			}).ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(claims.ID))
	}
}

func (a *Auth) PostAuth(db *sql.DB) http.HandlerFunc {
	a.SystemManager.DB = db
	return func(w http.ResponseWriter, r *http.Request) {
		id := uuid.Must(uuid.NewRandom())
		t, err := a.GenerateJWTauthToken(id.String(), "demo", time.Duration(60*time.Minute))
		if err != nil {
			common.FailMessage(types.FailMessage{
				Message:    "Failed to generated JWT for auth",
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(t))
	}
}

func main() {
	log.Println("Launching auth")
	runtime := &Auth{}
	systemManager := &system.SystemManager{}
	runtime.SystemManager = systemManager

	service.NewService().
		RegisterHandlerFunc(
			"/",
			runtime.GetAuth,
			&service.HandlerOptions{
				Methods: []string{http.MethodGet},
			}).
		RegisterHandlerFunc(
			"/login",
			runtime.PostAuth,
			&service.HandlerOptions{
				Methods: []string{http.MethodPost},
			}).
		DefaultMiddleware().
		ListenAndServe()
}
