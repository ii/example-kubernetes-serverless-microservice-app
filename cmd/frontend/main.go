package main

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"os"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/common"
)

type Frontend struct {
	BaseDomain string
}

func serveHTMLWithTemplating(filePath string) func(w http.ResponseWriter, r *http.Request) {
	var content []byte
	tmpl := template.Must(template.ParseFiles(os.Getenv("KO_DATA_PATH") + filePath))
	frontend := Frontend{
		BaseDomain: common.GetEnvValueOrDefault("APP_BASE_DOMAIN", "coolcoffee.company"),
	}
	templatedBuffer := new(bytes.Buffer)
	tmpl.Execute(templatedBuffer, frontend)
	content = templatedBuffer.Bytes()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write(content)
	})
}

func main() {
	log.Println("Launching frontend")
	server := http.NewServeMux()
	server.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir(os.Getenv("KO_DATA_PATH")+"/static"))))
	server.HandleFunc("/products", serveHTMLWithTemplating("/product.html"))
	server.HandleFunc("/", serveHTMLWithTemplating("/index.html"))
	m := common.Logging(server)
	handler := common.GetAppDefaultCorsPolicy().Handler(m)
	log.Fatal(http.ListenAndServe(":8080", handler))
}
