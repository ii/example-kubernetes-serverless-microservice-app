package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/common"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/service"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/types"
)

type ItemOptions struct {
	FilterByID string
}

type Inventory struct {
	DB *sql.DB
}

func (i *Inventory) getItems(options *ItemOptions) (items []types.Item, err error) {
	extraParams := []string{}
	extraArgs := []interface{}{}
	if options.FilterByID != "" {
		extraParams = append(extraParams, ` and id = $1 `)
		extraArgs = append(extraArgs, options.FilterByID)
	}

	sqlStatement := `
  select id, name, price::numeric::int, size, comment, creationTimestamp, modificationTimestamp, deletionTimestamp
  from product
  where deletionTimestamp = 0 `
	sqlStatement += strings.Join(extraParams, " ")
	sqlStatement += ` order by name`

	rows, err := i.DB.Query(sqlStatement, extraArgs...)
	if err != nil {
		log.Printf("Failed to query the database, %v", err)
		return []types.Item{}, fmt.Errorf("Failed to query the database")
	}
	defer rows.Close()
	for rows.Next() {
		var item types.Item
		rows.Scan(
			&item.ID,
			&item.Name,
			&item.Price,
			&item.Size,
			&item.Comment,
			&item.CreationTimestamp,
			&item.ModificationTimestamp,
			&item.DeletionTimestamp,
		)
		items = append(items, item)
	}
	return items, nil
}

func (i *Inventory) GetItems(db *sql.DB) http.HandlerFunc {
	i.DB = db
	return func(w http.ResponseWriter, r *http.Request) {
		items, err := i.getItems(&ItemOptions{})
		if err != nil {
			common.FailMessage(types.FailMessage{
				Message:    err.Error(),
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		itemsBytes, err := json.Marshal(items)
		if err != nil {
			log.Printf("Failed to marshal items to JSON, %v", err)
			common.FailMessage(types.FailMessage{
				Message:    "Failed to prepare content for sending",
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(itemsBytes)
	}
}

func (i *Inventory) GetItem(db *sql.DB) http.HandlerFunc {
	i.DB = db
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id, ok := vars["id"]
		if !ok {
			common.FailMessage(types.FailMessage{
				Message:    "ID not found in request path",
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}

		items, err := i.getItems(&ItemOptions{FilterByID: id})
		if err != nil {
			common.FailMessage(types.FailMessage{
				Message:    err.Error(),
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		if len(items) == 0 {
			common.FailMessage(types.FailMessage{
				Message:    "No items found",
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		itemsBytes, err := json.Marshal(items[0])
		if err != nil {
			log.Printf("Failed to marshal items to JSON, %v", err)
			common.FailMessage(types.FailMessage{
				Message:    "Failed to prepare content for sending",
				StatusCode: http.StatusInternalServerError,
			}).ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(itemsBytes)
	}
}

func main() {
	log.Println("Launching inventory")
	inventory := &Inventory{}
	service.NewService().
		RegisterHandlerFunc(
			"/",
			inventory.GetItems,
			&service.HandlerOptions{
				Methods: []string{http.MethodGet},
			}).
		RegisterHandlerFunc(
			"/{id}",
			inventory.GetItem,
			&service.HandlerOptions{
				Methods: []string{http.MethodGet},
			}).
		DefaultMiddleware().
		ListenAndServe()
}
