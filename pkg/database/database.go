package database

import (
	"database/sql"

	// include Pg
	_ "github.com/lib/pq"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/common"
)

type Database struct {
	ConnectionString string
	*sql.DB
}

func NewDatabase() *Database {
	return &Database{
		ConnectionString: common.GetAppDatabaseConnectionString(),
	}
}

// Open ...
// given database credentials, return a database connection
func (d *Database) Open() (*sql.DB, error) {
	return sql.Open("postgres", d.ConnectionString)
}

// Close ...
// close the connection to the database
func (d *Database) Close() (err error) {
	return d.Close()
}
