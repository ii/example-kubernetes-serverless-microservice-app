package common

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/rs/cors"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/types"
)

func GetEnvValueOrDefault(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value != "" {
		return value
	}
	return defaultValue
}

func GetAppDatabaseConnectionString() string {
	return GetEnvValueOrDefault("APP_DB_STRING",
		"postgres://postgres:postgres@postgres:5432/postgres?sslmode=disable&connect_timeout=2")
}

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v %v %v %v %v", r.Method, r.URL, r.Proto, r.Response, r.Header)
		next.ServeHTTP(w, r)
	})
}

func GetAppDefaultCorsPolicy() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedHeaders:   []string{"Accept", "Content-Type", "Authorization", "User-Agent", "Accept-Encoding"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowCredentials: true,
	})
}

func FailMessage(message types.FailMessage) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		messageBytes, _ := json.Marshal(message)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(message.StatusCode)
		w.Write(messageBytes)
	})
}
