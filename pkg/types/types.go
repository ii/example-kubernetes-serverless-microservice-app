package types

import (
	jwt "github.com/golang-jwt/jwt"
)

type JWTclaim struct {
	ID        string `json:"id"`
	AuthNonce string `json:"authNonce"`
	jwt.StandardClaims
}

type Item struct {
	ID                    string  `json:"id"`
	Name                  string  `json:"name"`
	Price                 float64 `json:"price"`
	Size                  string  `json:"size"`
	Comment               string  `json:"comment"`
	CreationTimestamp     int     `json:"creationTimestamp"`
	ModificationTimestamp int     `json:"modificationTimestamp"`
	DeletionTimestamp     int     `json:"deletionTimestamp"`
}

type FailMessage struct {
	Message    string
	StatusCode int
}
