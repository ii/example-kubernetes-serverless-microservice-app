package service

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/common"
	"gitlab.com/ii/example-kubernetes-serverless-microservice-app/pkg/database"
)

type Service struct {
	DB   *sql.DB
	Port string

	router *mux.Router
	server http.Handler
}

type HandlerOptions struct {
	Methods []string
}

var defaultMethods = []string{http.MethodGet}

func SetDefaultMethods(input []string) (output []string) {
	if len(input) == 0 {
		return defaultMethods
	}
	return input
}

type MiddlewareHandler func(*sql.DB) mux.MiddlewareFunc
type HandlerFunc func(*sql.DB) http.HandlerFunc
type Handler func(*sql.DB) http.Handler

func NewService() *Service {
	db, err := database.NewDatabase().Open()
	if err != nil {
		panic(err)
	}
	router := mux.NewRouter()

	return &Service{
		DB:     db,
		Port:   ":8080",
		router: router,
		server: router,
	}
}

func (s *Service) RegisterHandler(path string, handler Handler, options *HandlerOptions) *Service {
	options.Methods = SetDefaultMethods(options.Methods)
	s.router.Handle(path, handler(s.DB)).Methods(options.Methods...)
	return s
}

func (s *Service) RegisterHandlerFunc(path string, handlerFunc HandlerFunc, options *HandlerOptions) *Service {
	options.Methods = SetDefaultMethods(options.Methods)
	s.router.HandleFunc(path, handlerFunc(s.DB)).Methods(options.Methods...)
	return s
}

func (s *Service) RegisterMiddleware(middleware MiddlewareHandler, options *HandlerOptions) *Service {
	s.router.Use(middleware(s.DB))
	return s
}

func (s *Service) DefaultMiddleware() *Service {
	m := common.Logging(s.router)
	handler := common.GetAppDefaultCorsPolicy().Handler(m)
	s.server = handler
	return s
}

func (s *Service) ListenAndServe() {
	log.Fatal(http.ListenAndServe(s.Port, s.server))
}
