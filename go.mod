module gitlab.com/ii/example-kubernetes-serverless-microservice-app

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.0
	github.com/rs/cors v1.8.2
)
