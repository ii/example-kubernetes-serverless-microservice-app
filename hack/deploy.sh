#!/usr/bin/env bash

cd $(git rev-parse --show-toplevel)

if [ "$1" = "uninstall" ]; then
    ko delete -f config/
    exit $?
fi
ko delete -f config/02-migrator.yaml 2> /dev/null || true
ko apply --local -f config/
