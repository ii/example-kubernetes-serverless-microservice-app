#!/bin/bash

cd $(git rev-parse --show-toplevel)

OUTPUT_FOLDER=cmd/frontend/kodata/static/ext

rm -rf "${OUTPUT_FOLDER}"
mkdir -p "${OUTPUT_FOLDER}"

SCRIPTS=(
    https://unpkg.com/axios@0.26.1/dist/axios.min.js
)

cd "${OUTPUT_FOLDER}"
for SCRIPT in ${SCRIPTS[*]}; do
    curl -O -L -s "${SCRIPT}"
done
